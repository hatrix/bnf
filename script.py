#!/usr/bin/env python3

from urllib.request import urlretrieve
from subprocess import Popen
import requests
import os

def get_proper_url(url):
    r = requests.get(url)
    doc_id = r.text.split("var images = '")[1].split("';")[0]

    return "http://gallica.bnf.fr/proxy?method=R&ark={}&l=7&r=".format(doc_id)


def get_max(url, step):
    print("Retrieving size of the map…")
    w, h = 0, 0

    i = 0
    while True:
        r = requests.get(url + "{0},{1},{2},{2}".format(h, i * step, step))
        i += 1
        if "Whitelabel Error Page" in r.text:
            break
    max_w = i

    i = 0
    while True:
        r = requests.get(url + "{0},{1},{2},{2}".format(i * step, w, step))
        i += 1
        if "Whitelabel Error Page" in r.text:
            break
    max_h = i 

    print("Done! {}x{} ({}x{} tiles)".format(max_w * step, max_h * step, 
                                             max_w, max_h))
    return max_w, max_h


def download(max_w, max_h, step):
    steps_h = [step * i for i in range(max_h)]
    steps_w = [step * i for i in range(max_w)]
    
    i = 0
    for h in steps_h:
        for w in steps_w:
            i += 1
            print("Saving image {} on {}".format(i, max_h * max_w))

            def_url = url + "{0},{1},{2},{2}".format(h, w, step)
            urlretrieve(def_url, "./image_{}_{}.jpg".format(h, w))


# URL de la carte à télécharger
url = input("Url: ")
url = get_proper_url(url)

# Nettoyage du dossier
[os.remove(f) for f in os.listdir('./') if 'image_' in f]

# Taille de la carte
step = 1024
max_w, max_h = get_max(url, step)

# Téléchargement !
download(max_w, max_h, step)

# Assemblage avec ImageMagick
Popen(['montage', 'image_*', '-geometry', '+0+0', 
       '-tile', '{}x{}'.format(max_w, max_h), 'result.jpg'])
